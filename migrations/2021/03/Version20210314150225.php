<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210314150225 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $translations = [
            ['original' => 'event-with-childrens.overview', 'hash' => '3bb23a9a27d8f2e970d7b21343fd3560', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Akce s dětmi', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-with-childrens.overview.title', 'hash' => '93e7f97ddd183a25de9fbbcc63d61369', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Akce s dětmi|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.event-with-childrens.overview.action.new', 'hash' => '4d4ca1c8171f871ef7a7fc4db7661fa0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit akci s dětmi', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.event-with-childrens.overview.name', 'hash' => '2277076afc63bc264f91b31d62b60c30', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.event-with-childrens.overview.place-name', 'hash' => 'f48ad699709820a352df942233127d80', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Místo konání', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.event-with-childrens.overview.term', 'hash' => '5825b70b9574a9e536fa932c1ce713d0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Termín', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.event-with-childrens.overview.number-of-children', 'hash' => 'f6280485bede4a0c00bed121db39520d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet dětí', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.event-with-childrens.overview.number-of-staff', 'hash' => 'c4cbb81d5a47783d89d01901c792d028', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet personálu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.event-with-childrens.overview.total-number-of-people', 'hash' => '6f6f3c6456e87ed547f3b41382fd5560', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Celkový počet', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-with-childrens.edit.title', 'hash' => '610f466b4a58a1bca75c3a12f9d2fbfc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení akce s dětmi', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.name', 'hash' => 'cfcf24f3a49b399eb88bdf91499c976f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.name.req', 'hash' => 'df635692d49e8062ae049105f1d90c50', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.number-of-children', 'hash' => '3593ee9286a6fec1c16e5ebc3b4787a3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet dětí', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.number-of-children.req', 'hash' => '25787071d7946ff918a464a207da8028', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím počet dětí', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.term', 'hash' => 'c16bfd5ac01a8568a239ec364fa9e0b2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Termín', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.term.req', 'hash' => '884bb19ee29e23dd67550e7c5559bf59', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím termín', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.place-name', 'hash' => 'ce02e8e412eedbe5998c9feb96e5196a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název místa konání', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.place-address', 'hash' => 'cc8962b85524efb6d882ee23fd4fad4f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Adresa', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.place-gps-lat', 'hash' => 'bae99844fe1823b3b50c99281f8866f8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'GPS šířka', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.place-gps-lng', 'hash' => '47172ac7e9cf667349d9600569c4cffc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'GPS výška', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.image-preview', 'hash' => 'b18c3d212734667eaf9c198774ab16cf', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled akce s dětmi', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.image-preview.rule-image', 'hash' => 'd4a736e080450f511d731ded61163943', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.content', 'hash' => 'f4482aebe94e90ca6a710ffc487e22d8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.send', 'hash' => '5e51f8dcefab7cd89176ba476b5baf3a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.send-back', 'hash' => 'c58c813008c17194beaec97e5d10cb03', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.back', 'hash' => '5458c4be6defeb9499e8ba1bb15b56d8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-with-childrens.edit.title - %s', 'hash' => 'efcfa1354cafa9bb37d41829caabe60a', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace akce s dětmi', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.flash.success.create', 'hash' => 'dcc471ec5d0f26298a1af757775ee6d9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Akce s dětmi byla úspěšně založena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.event-with-childrens.staff.action.add', 'hash' => '14ab609ceac48e70565278f865d71297', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přidat personál', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.event-with-childrens.staff.name', 'hash' => 'd36e330c61145836dec0cdfaeca1144c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pozice', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.event-with-childrens.staff.description', 'hash' => '15f1c0247bf63735c0deb8b55598557e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.event-with-childrens.staff.number', 'hash' => '073421e65ecf9fde113dadf2eff1fe2f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.event-with-childrens.staff.financial-reward', 'hash' => '9e3cc8f7b6292b7a2c5e46cc60ec95a0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odměna', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.event-with-childrens.staff.financial-premium', 'hash' => '5d5994c5d09cfb38e47c953462a902c2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Prémie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.flash.success.update', 'hash' => '2eb727244f5d0f52c4ba40f08e6d7bd6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Akce s dětmi byla úspěšně upravena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.file.name', 'hash' => 'f9790944a349e4692ed6a36bb38a3833', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.file.size', 'hash' => '3e31812057285dbb889f857b813617b7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Velikost', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.file.last-update-at', 'hash' => '60e1162a735e2136f6eb715866bb9649', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Upraveno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.file.action.download', 'hash' => '96080632f6f2b3c56b1ecc46ad52615b', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.file.remove.confirm', 'hash' => '400a8f5f815f21dd8480a1c5f3d5b895', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Opravdu chcete odebrat dokument?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.event-with-childrens.edit.file.action.remove', 'hash' => 'a57e6eaed1658e2e60e1764c3c55700a', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.event-with-childrens.title', 'hash' => 'a133628eb38081565a8196a4fc78d339', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Akce s dětmi', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.event-with-childrens.description', 'hash' => 'c7edb6d8db7422508bd7cc6be25124f7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat akce s dětmi', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.event-with-childrens.overview.action.edit', 'hash' => '0f898024cf0c85b68d945de753908164', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.event-with-childrens.overview.action.detail', 'hash' => 'd09d5f172e9616fe24058185e9a5998a', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.title - %s', 'hash' => 'e09ebb8ddda37bc51bdfb69619fefa99', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Detail akce s dětmi', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.term', 'hash' => '15b1c989540184f4b44520d4a693c70a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Termín', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.number-of-children', 'hash' => '652aeed3c4644ef43780963301ef6fe8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet dětí', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.number-of-staff', 'hash' => '8bd8bfc7b308b6b6f104a216b569072a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet personálu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.total-number-of-people', 'hash' => '89b6eee5c3b9069f84f0a1902d40e277', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Celkový počet', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.staff.title', 'hash' => '2c2cf0451a462d41b766a7ef2bac532c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Personál', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.staff.name', 'hash' => '94b5ea94917b23a70ea1ebb7746a37cc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pozice', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.staff.financial-reward', 'hash' => 'fef807867a7f2e7846eddc8d1036aff8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odměna', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.staff.financial-premium', 'hash' => '584c6ededd029796e8c12a89fda3ae82', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Prémie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.staff.logged', 'hash' => 'e53976d69ce597ddfaba5e5736a65b30', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obsazenost', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.staff.log-in', 'hash' => 'e0f51f52fd514ad8575b12b8236825ed', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přihlásit se', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.content.title', 'hash' => 'f5b742e4fffc171694e865a7aea13703', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.files.title', 'hash' => '9bc7b06ccf42d4f9c68f255415da384f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Dokumenty', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.file.name', 'hash' => '3d19fa6de5b9ba87e0457d7be95375ed', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.file.size', 'hash' => 'ae5c969630cf87c6c055ef9591874e2f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Velikost', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.file.last-update-at', 'hash' => '954e5ba0094611c5e47cfa1e7e5bc08e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Upraveno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.file.action.download', 'hash' => 'b6d6aa40dba01a0c4e2eae2325012bf3', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.flash.success.login', 'hash' => '22a7fe24e2b42a5037b70cf7f74915d6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Úspěšně jste se přihlásili.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.staff.log-out-user.confirm', 'hash' => 'f5e3a4e58fcaed90bda2c831e136707f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Opravdu chcete odhlásit účastníka?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.staff.log-out.confirm', 'hash' => '78f2a526629b42615cdb4ca2452d0d78', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Opravdu se chcete odhlásit?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.staff.log-out', 'hash' => '750ceca4bdf3f71c8f4ec2b9a118ab89', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odhlásit se', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.flash.success.logout', 'hash' => 'e5ca7258729d9eeb1390a241750ff4ae', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Úspěšně jste se odhlásili.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.flash.success.logout-user', 'hash' => 'de5e0d4df5000845900cce5ff3b982a2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Úspěšně jste odhlásili účastníka.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.event-with-childrens.detail.place', 'hash' => '30a2ead694f1610ce50f9ef174cec02c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Adresa', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
