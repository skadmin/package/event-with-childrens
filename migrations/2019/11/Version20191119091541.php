<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191119091541 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE event_with_childrens_file (event_with_childrens_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_131B710172ADE970 (event_with_childrens_id), INDEX IDX_131B710193CB796C (file_id), PRIMARY KEY(event_with_childrens_id, file_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE event_with_childrens_file ADD CONSTRAINT FK_131B710172ADE970 FOREIGN KEY (event_with_childrens_id) REFERENCES event_with_childrens (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE event_with_childrens_file ADD CONSTRAINT FK_131B710193CB796C FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE event_with_childrens_file');
    }
}
