<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Skadmin\EventWithChildrens\BaseControl;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191114115307 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $data = [
            'base_control' => BaseControl::class,
            'webalize'     => 'event-with-childrens',
            'name'         => 'Event with childrens',
            'is_active'    => 1,
        ];

        $this->addSql(
            'INSERT INTO core_package (base_control, webalize, name, is_active) VALUES (:base_control, :webalize, :name, :is_active)',
            $data
        );

        $resources = [
            [
                'name'        => 'event-with-childrens',
                'title'       => 'role-resource.event-with-childrens.title',
                'description' => 'role-resource.event-with-childrens.description',
            ],
        ];

        foreach ($resources as $resource) {
            $this->addSql('INSERT INTO core_role_resource (name, title, description) VALUES (:name, :title, :description)', $resource);
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
