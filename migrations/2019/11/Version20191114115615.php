<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191114115615 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE event_with_childrens (id INT AUTO_INCREMENT NOT NULL, number_of_children INT NOT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, term_from DATETIME NOT NULL, term_to DATETIME NOT NULL, place_name VARCHAR(255) NOT NULL, place_address VARCHAR(255) NOT NULL, place_gps_lat VARCHAR(255) NOT NULL, place_gps_lng VARCHAR(255) NOT NULL, image_preview VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event_with_childrens_staff (id INT AUTO_INCREMENT NOT NULL, event_with_childrens_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, number INT NOT NULL, financial_reward INT NOT NULL, financial_premium VARCHAR(255) NOT NULL, INDEX IDX_2841572772ADE970 (event_with_childrens_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event_with_childrens_staff_user (staff_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_D9882B00D4D57CD (staff_id), INDEX IDX_D9882B00A76ED395 (user_id), PRIMARY KEY(staff_id, user_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE event_with_childrens_staff ADD CONSTRAINT FK_2841572772ADE970 FOREIGN KEY (event_with_childrens_id) REFERENCES event_with_childrens (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE event_with_childrens_staff_user ADD CONSTRAINT FK_D9882B00D4D57CD FOREIGN KEY (staff_id) REFERENCES event_with_childrens_staff (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE event_with_childrens_staff_user ADD CONSTRAINT FK_D9882B00A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE event_with_childrens_staff DROP FOREIGN KEY FK_2841572772ADE970');
        $this->addSql('ALTER TABLE event_with_childrens_staff_user DROP FOREIGN KEY FK_D9882B00D4D57CD');
        $this->addSql('DROP TABLE event_with_childrens');
        $this->addSql('DROP TABLE event_with_childrens_staff');
        $this->addSql('DROP TABLE event_with_childrens_staff_user');
    }
}
