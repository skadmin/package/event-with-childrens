<?php

declare(strict_types=1);

namespace Skadmin\EventWithChildrens\Doctrine\EventWithChildrens;

use SkadminUtils\DoctrineTraits\Entity;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Skadmin\EventWithChildrens\Doctrine\Staff\Staff;
use Skadmin\File\Doctrine\File\File;
use Doctrine\ORM\Mapping as ORM;

use function ceil;
use function intval;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class EventWithChildrens
{
    use Entity\Id;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\Term;
    use Entity\PlaceOnTheMap;
    use Entity\ImagePreview;

    #[ORM\Column]
    private int $numberOfChildren = 0;

    /** @var ArrayCollection<int, Staff> */
    #[ORM\OneToMany(targetEntity: Staff::class, mappedBy: 'eventWithChildrens')]
    private $staff;

    /** @var ArrayCollection<int, File> */
    #[ORM\ManyToMany(targetEntity: File::class)]
    private $files;

    private ?int $numberOfStaff = null;

    private ?int $numberOfLoggedStaff = null;

    public function __construct()
    {
        $this->staff = new ArrayCollection();
        $this->files = new ArrayCollection();
    }

    public function getTotalNumberOfPeople(): int
    {
        return $this->getNumberOfChildren() + $this->getNumberOfStaff();
    }

    public function getNumberOfChildren(): int
    {
        return $this->numberOfChildren;
    }

    public function getNumberOfStaff(): int
    {
        if ($this->numberOfStaff === null) {
            $this->numberOfStaff = 0;

            /** @var Staff $staff */
            foreach ($this->staff as $staff) {
                $this->numberOfStaff += $staff->getNumber();
            }
        }

        return $this->numberOfStaff;
    }

    public function getPercentLoggedStaff(): int
    {
        if ($this->getNumberOfStaff() > 0) {
            return intval(ceil(100 / $this->getNumberOfStaff() * $this->getNumberOfLoggedStaff()));
        }
        return 100;
    }

    public function getNumberOfLoggedStaff(): int
    {
        if ($this->numberOfLoggedStaff === null) {
            $this->numberOfLoggedStaff = 0;

            /** @var Staff $staff */
            foreach ($this->staff as $staff) {
                $this->numberOfLoggedStaff += $staff->getNumberOfLoggedUsers();
            }
        }

        return $this->numberOfLoggedStaff;
    }

    /**
     * @return ArrayCollection|Staff[]
     */
    public function getStaff()
    {
        return $this->staff;
    }

    /**
     * @return ArrayCollection|File[]
     */
    public function getFiles()
    {
        return $this->files;
    }

    public function update(string $name, int $numberOfChildren, string $content, DateTimeInterface $termFrom, DateTimeInterface $termTo, string $placeName, string $placeAddress, string $placeGpsLat, string $placeGpsLng, ?string $imagePreview): void
    {
        $this->name             = $name;
        $this->numberOfChildren = $numberOfChildren;
        $this->content          = $content;
        $this->termFrom         = $termFrom;
        $this->termTo           = $termTo;

        $this->placeName    = $placeName;
        $this->placeAddress = $placeAddress;
        $this->placeGpsLat  = $placeGpsLat;
        $this->placeGpsLng  = $placeGpsLng;

        if ($imagePreview !== null && $imagePreview !== '') {
            $this->imagePreview = $imagePreview;
        }
    }

    public function addFile(File $file): void
    {
        $this->files->add($file);
    }

    public function removeFileByHash(string $hash): void
    {
        $file = $this->getFileByHash($hash);

        if ($file !== null) {
            $this->files->removeElement($file);
        }
    }

    public function getFileByHash(string $hash): ?File
    {
        $criteria = Criteria::create()->where(Criteria::expr()->eq('hash', $hash));
        $file     = $this->files->matching($criteria)->first();

        return $file ? $file : null;
    }
}
