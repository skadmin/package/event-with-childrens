<?php

declare(strict_types=1);

namespace Skadmin\EventWithChildrens\Doctrine\EventWithChildrens;

use SkadminUtils\DoctrineTraits\Facade;
use DateTimeInterface;
use Nette\Utils\DateTime;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\File\Doctrine\File\File;

/**
 * Class EventWithChildrensFacade
 */
final class EventWithChildrensFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = EventWithChildrens::class;
    }

    public function create(string $name, int $numberOfChildren, string $content, DateTimeInterface $termFrom, DateTimeInterface $termTo, string $placName, string $placeAddress, string $placeGpsLat, string $placeGpsLng, ?string $imagePreview) : EventWithChildrens
    {
        return $this->update(null, $name, $numberOfChildren, $content, $termFrom, $termTo, $placName, $placeAddress, $placeGpsLat, $placeGpsLng, $imagePreview);
    }

    public function update(?int $id, string $name, int $numberOfChildren, string $content, DateTimeInterface $termFrom, DateTimeInterface $termTo, string $placName, string $placeAddress, string $placeGpsLat, string $placeGpsLng, ?string $imagePreview) : EventWithChildrens
    {
        $article = $this->get($id);
        $article->update($name, $numberOfChildren, $content, $termFrom, $termTo, $placName, $placeAddress, $placeGpsLat, $placeGpsLng, $imagePreview);

        $this->em->persist($article);
        $this->em->flush();

        return $article;
    }

    public function get(?int $id = null) : EventWithChildrens
    {
        if ($id === null) {
            return new EventWithChildrens();
        }

        $eventWithChildrens = parent::get($id);

        if ($eventWithChildrens === null) {
            return new EventWithChildrens();
        }

        return $eventWithChildrens;
    }

    /**
     * @return EventWithChildrens[]
     */
    public function getEventsWithChildrensInFuture(?int $limit = null) : array
    {
        $qb = $this->getModel();
        $qb->where('a.termFrom >= :termFrom')
            ->setParameter('termFrom', (new DateTime())->setTime(0, 0, 0))
            ->orderBy('a.termFrom');

        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return EventWithChildrens[]
     */
    public function getEventsWithChildrensInPast(?int $limit = null) : array
    {
        $qb = $this->getModel();
        $qb->where('a.termTo < :termTo')
            ->setParameter('termTo', (new DateTime())->setTime(0, 0, 0))
            ->orderBy('a.termTo', 'DESC');

        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    public function addFile(EventWithChildrens $eventWithChildrens, File $file) : void
    {
        $eventWithChildrens->addFile($file);
        $this->em->flush();
    }

    public function getFileByHash(EventWithChildrens $eventWithChildrens, string $hash) : ?File
    {
        return $eventWithChildrens->getFileByHash($hash);
    }

    public function removeFileByHash(EventWithChildrens $eventWithChildrens, string $hash) : void
    {
        $eventWithChildrens->removeFileByHash($hash);
        $this->em->flush();
    }
}
