<?php

declare(strict_types=1);

namespace Skadmin\EventWithChildrens\Doctrine\Staff;

use SkadminUtils\DoctrineTraits\Entity;
use App\Model\Doctrine\User\User;
use Doctrine\Common\Collections\ArrayCollection;
use Skadmin\EventWithChildrens\Doctrine\EventWithChildrens\EventWithChildrens;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'event_with_childrens_staff')]
#[ORM\HasLifecycleCallbacks]
class Staff
{
    use Entity\Id;
    use Entity\Name;
    use Entity\Description;
    use Entity\Number;
    use Entity\FinancialReward;
    use Entity\FinancialPremium;

    #[ORM\ManyToOne(targetEntity: EventWithChildrens::class, inversedBy: 'staff')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private EventWithChildrens $eventWithChildrens;

    /** @var ArrayCollection<int, User> */
    #[ORM\ManyToMany(targetEntity: User::class)]
    #[ORM\JoinTable(name: 'event_with_childrens_staff_user')]
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function update(EventWithChildrens $eventWithChildrens, string $name, string $description, int $number, int $financialReward, string $financialPremium) : void
    {
        $this->eventWithChildrens = $eventWithChildrens;
        $this->name               = $name;
        $this->description        = $description;
        $this->number             = $number;

        $this->financialReward  = $financialReward;
        $this->financialPremium = $financialPremium;
    }

    public function addUser(User $user) : bool
    {
        if ($this->isUserLoggedInEventWithChildrens($user)) {
            return false;
        }

        if ($this->getNumberOfLoggedUsers() >= $this->getNumber()) {
            return false;
        }

        $this->users->add($user);
        return true;
    }

    public function isUserLoggedInEventWithChildrens(User $user) : bool
    {
        /** @var Staff $staff */
        foreach ($this->getEventWithChildrens()->getStaff() as $staff) {
            if ($staff->getUsers()->contains($user)) {
                return true;
            }
        }

        return false;
    }

    public function getEventWithChildrens() : EventWithChildrens
    {
        return $this->eventWithChildrens;
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    public function getNumberOfLoggedUsers() : int
    {
        return $this->users->count();
    }

    public function removeUser(User $user) : void
    {
        $this->users->removeElement($user);
    }

    public function isUserLogged(User $user) : bool
    {
        return $this->users->contains($user);
    }
}
