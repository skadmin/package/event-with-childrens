<?php

declare(strict_types=1);

namespace Skadmin\EventWithChildrens;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'event-with-childrens';
    public const DIR_IMAGE = 'event-with-childrens';

    public function getMenu() : ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-calendar-day']),
            'items'   => ['overview'],
        ]);
    }
}
