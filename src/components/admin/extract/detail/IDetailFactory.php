<?php

declare(strict_types=1);

namespace Skadmin\EventWithChildrens\Components\Admin;

/**
 * Interface IDetailFactory
 */
interface IDetailFactory
{
    public function create(int $id) : Detail;
}
