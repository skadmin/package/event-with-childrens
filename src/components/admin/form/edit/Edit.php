<?php

declare(strict_types=1);

namespace Skadmin\EventWithChildrens\Components\Admin;

use App\Components\Form\FormWithUserControl;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use SkadminUtils\ImageStorage\ImageStorage;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nette\Utils\DateTime;
use Skadmin\EventWithChildrens\BaseControl;
use Skadmin\EventWithChildrens\Doctrine\EventWithChildrens\EventWithChildrens;
use Skadmin\EventWithChildrens\Doctrine\EventWithChildrens\EventWithChildrensFacade;
use Skadmin\File\Components\Admin\CreateComponentFormFile;
use Skadmin\File\Components\Admin\FileDownloadByFacade;
use Skadmin\File\Components\Admin\FileRemoveByFacade;
use Skadmin\File\Components\Admin\FormFile;
use Skadmin\File\Components\Admin\IFormFileFactory;
use Skadmin\FileStorage\FileStorage;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;
use function explode;
use function intval;
use function is_bool;

/**
 * Class Edit
 */
class Edit extends FormWithUserControl
{
    use APackageControl;
    use CreateComponentFormFile;
    use FileDownloadByFacade {
        CreateComponentFormFile::checkFileObject insteadof FileDownloadByFacade;
    }
    use FileRemoveByFacade {
        CreateComponentFormFile::checkFileObject insteadof FileRemoveByFacade;
    }

    /** @var IStaffFactory */
    public $iStaffFactory;

    /** @var LoaderFactory */
    public $webLoader;

    /** @var EventWithChildrensFacade */
    private $facade;

    /** @var EventWithChildrens */
    private $eventWithChildrens;

    /** @var ImageStorage */
    private $imageStorage;

    /** @var FormFile */
    private $formFile;

    public function __construct(?int $id, EventWithChildrensFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, IStaffFactory $iStaffFactory, ImageStorage $imageStorage, IFormFileFactory $iFormFileFactory, FileStorage $fileStorage)
    {
        parent::__construct($translator, $user);
        $this->facade       = $facade;
        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->eventWithChildrens = $this->facade->get($id);
        $this->fileObject         = $this->eventWithChildrens;
        $this->fileStorage        = $fileStorage;

        $this->iStaffFactory = $iStaffFactory;

        if (! $this->eventWithChildrens->isLoaded()) {
            return;
        }

        $this->formFile = $iFormFileFactory->create();
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return SimpleTranslation|string
     */
    public function getTitle()
    {
        if ($this->eventWithChildrens->isLoaded()) {
            return new SimpleTranslation('event-with-childrens.edit.title - %s', $this->eventWithChildrens->getName());
        }

        return 'event-with-childrens.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss() : array
    {
        $css = [
            $this->webLoader->createCssLoader('daterangePicker'),
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];

        if ($this->eventWithChildrens->isLoaded()) {
            foreach ($this->formFile->getCss() as $wl) {
                $css[] = $wl;
            }
        }

        return $css;
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        $js = [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('moment'),
            $this->webLoader->createJavaScriptLoader('daterangePicker'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];

        if ($this->eventWithChildrens->isLoaded()) {
            foreach ($this->formFile->getJs() as $wl) {
                $js[] = $wl;
            }
        }

        return $js;
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        /**
         * @var DateTime $termFrom
         * @var DateTime $termTo
         */
        [$termFrom, $termTo] = Arrays::map(explode(' - ', $values->term), static function (string $date) : DateTime {
            $date = DateTime::createFromFormat('d.m.Y', $date);
            return is_bool($date) ? new DateTime() : $date;
        });

        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->image_preview, BaseControl::DIR_IMAGE);

        if ($this->eventWithChildrens->isLoaded()) {
            if ($identifier !== null && $this->eventWithChildrens->getImagePreview() !== null) {
                $this->imageStorage->delete($this->eventWithChildrens->getImagePreview());
            }

            $eventWithChildrens = $this->facade->update(
                $this->eventWithChildrens->getId(),
                $values->name,
                intval($values->number_of_children),
                $values->content,
                $termFrom,
                $termTo,
                $values->place_name,
                $values->place_address,
                $values->place_gps_lat,
                $values->place_gps_lng,
                $identifier
            );
            $this->onFlashmessage('form.event-with-childrens.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $eventWithChildrens = $this->facade->create(
                $values->name,
                intval($values->number_of_children),
                $values->content,
                $termFrom,
                $termTo,
                $values->place_name,
                $values->place_address,
                $values->place_gps_lat,
                $values->place_gps_lng,
                $identifier
            );
            $this->onFlashmessage('form.event-with-childrens.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $eventWithChildrens->getId(),
        ]);
    }

    public function processOnBack() : void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    public function render() : void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->eventWithChildrens = $this->eventWithChildrens;
        $template->render();
    }

    protected function createComponentForm() : Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.event-with-childrens.edit.name')
            ->setRequired('form.event-with-childrens.edit.name.req');
        $form->addTextArea('content', 'form.event-with-childrens.edit.content');

        // EventWithChildrens
        $form->addText('number_of_children', 'form.event-with-childrens.edit.number-of-children')
            ->setRequired('form.event-with-childrens.edit.number-of-children.req')
            ->setHtmlType('number')
            ->setHtmlAttribute('min', 0);
        $form->addText('term', 'form.event-with-childrens.edit.term')
            ->setRequired('form.event-with-childrens.edit.term.req')
            ->setHtmlAttribute('data-daterange');
        $form->addImageWithRFM('image_preview', 'form.event-with-childrens.edit.image-preview');

        // PLACE
        $form->addText('place_name', 'form.event-with-childrens.edit.place-name');
        $form->addText('place_address', 'form.event-with-childrens.edit.place-address');
        $form->addText('place_gps_lat', 'form.event-with-childrens.edit.place-gps-lat');
        $form->addText('place_gps_lng', 'form.event-with-childrens.edit.place-gps-lng');

        // BUTTON
        $form->addSubmit('send', 'form.event-with-childrens.edit.send');
        $form->addSubmit('send_back', 'form.event-with-childrens.edit.send-back');
        $form->addSubmit('back', 'form.event-with-childrens.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults() : array
    {
        if (! $this->eventWithChildrens->isLoaded()) {
            return [];
        }

        return [
            'name'               => $this->eventWithChildrens->getName(),
            'content'            => $this->eventWithChildrens->getContent(),
            'number_of_children' => $this->eventWithChildrens->getNumberOfChildren(),
            'term'               => $this->eventWithChildrens->getTermFromTo(),
            'place_name'         => $this->eventWithChildrens->getPlaceName(),
            'place_address'      => $this->eventWithChildrens->getPlaceAddress(),
            'place_gps_lat'      => $this->eventWithChildrens->getPlaceGpsLat(),
            'place_gps_lng'      => $this->eventWithChildrens->getPlaceGpsLng(),
        ];
    }

    protected function createComponentStaff() : Staff
    {
        return $this->iStaffFactory->create($this->eventWithChildrens);
    }
}
