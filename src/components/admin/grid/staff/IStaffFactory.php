<?php

declare(strict_types=1);

namespace Skadmin\EventWithChildrens\Components\Admin;

use Skadmin\EventWithChildrens\Doctrine\EventWithChildrens\EventWithChildrens;

/**
 * Interface IOverviewFactory
 */
interface IStaffFactory
{
    public function create(EventWithChildrens $eventWithChildrens) : Staff;
}
