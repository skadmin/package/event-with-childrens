<?php

declare(strict_types=1);

namespace Skadmin\EventWithChildrens\Components\Front;

use App\Components\Grid\TemplateControl;
use App\Model\System\APackageControl;
use SkadminUtils\ImageStorage\ImageStorage;
use Skadmin\EventWithChildrens\BaseControl;
use Skadmin\EventWithChildrens\Doctrine\EventWithChildrens\EventWithChildrensFacade;
use Skadmin\Translator\Translator;

/**
 * Class Detail
 */
class Overview extends TemplateControl
{
    use APackageControl;

    /** @var EventWithChildrensFacade */
    private $facade;

    /** @var ImageStorage */
    private $imageStorage;

    public function __construct(EventWithChildrensFacade $facade, Translator $translator, ImageStorage $imageStorage)
    {
        parent::__construct($translator);
        $this->facade       = $facade;
        $this->imageStorage = $imageStorage;
    }

    public function getTitle(): string
    {
        return 'event-with-childrens.front.overview.title';
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');

        $template->eventsWithChildrens    = $this->facade->getEventsWithChildrensInFuture();
        $template->oldEventsWithChildrens = $this->facade->getEventsWithChildrensInPast();
        $template->package                = new BaseControl();

        $template->render();
    }
}
