<?php

declare(strict_types=1);

namespace Skadmin\EventWithChildrens\Components\Front;

/**
 * Interface IOverviewFactory
 */
interface IOverviewFactory
{
    public function create() : Overview;
}
